package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.graphics.Color;

public class BgBlackWhiteImpl implements Background{

	int white = Color.WHITE;
	int black = Color.BLACK;
	int currColor = Color.GRAY;
	int timer = 0;

	boolean toWhite;

	public void updatePhysics() {
		timer++;
		if (toWhite) {
			currColor += 0x00010101;
			if (currColor == Color.WHITE)
				toWhite = false;
		} else {
			currColor -= 0x00010101;
			if (currColor == Color.BLACK)
				toWhite = true;
		}
	}

	public void draw(Canvas c) {
		c.drawColor(currColor);
	}
}