package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.view.MotionEvent;

public class Gradients {

	final int TYPE_GRADIENTS = 5;
	final int MAX_GRADIENTS = 10;

	Gradient[] gradients = new Gradient[MAX_GRADIENTS];
	Gradient tempGradient;
	boolean[] exists = new boolean[MAX_GRADIENTS];
	int radius;

	public void actionDown(MotionEvent event) {
		int downX = (int) event.getX();
		int downY = (int) event.getY();

		boolean found = false;
		for (int i = 0; i < exists.length; i++) {
			if (!exists[i]) {
				exists[i] = true;
				tempGradient = new Gradient(radius);
				tempGradient.createGradient(downX, downY, i % TYPE_GRADIENTS);
				gradients[i] = tempGradient;
				found = true;
				break;
			}
		}

		if (!found) {
			for (int i = 0; i < exists.length; i++) {
				exists[i] = false;
				gradients[i] = null;
			}
		}
	}

	public void updatePhysics() {
		for (int i = 0; i < exists.length; i++) {
			if (exists[i]) {
				gradients[i].updatePhysics();
			}
		}
	}

	public void draw(Canvas canvas) {
		for (int i = 0; i < exists.length; i++) {
			if (exists[i]) {
				gradients[i].draw(canvas);
			}
		}
	}

	public void onSurfaceChanged(int x) {
		// x is the longest side of screen
		radius = x / 2;
	}
}