package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MainActivity extends WallpaperService {

	private final Handler mHandler = new Handler();

	@Override
	public void onCreate() {
		super.onCreate();
		android.os.Debug.waitForDebugger();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public Engine onCreateEngine() {
		return new MainEngine();
	}

	class MainEngine extends Engine {

		private boolean mVisible;
		private float mOffset;

		// Gradients gradients = new Gradients();
		TreeScene treeScene = new TreeScene();
		Background background = new BgBlackWhiteImpl();
		PlotPoints plot = new PlotPoints();
		// RectSet rectSet = new RectSet();

		private final Runnable drawFrame = new Runnable() {
			public void run() {
				drawFrame();
			}
		};

		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
			setTouchEventsEnabled(true);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			mHandler.removeCallbacks(drawFrame);
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			mVisible = visible;
			if (visible) {
				drawFrame();
			} else {
				mHandler.removeCallbacks(drawFrame);
			}
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			super.onSurfaceChanged(holder, format, width, height);
			// rectSet.onSurfaceChanged(plot, width, height);
			treeScene.onSurfaceChanged(plot, width, height);
			// if (height > width)
			// gradients.onSurfaceChanged(height);
			// else
			// gradients.onSurfaceChanged(width);

			drawFrame();
		}

		@Override
		public void onOffsetsChanged(float xOffset, float yOffset, float xStep,
				float yStep, int xPixels, int yPixels) {
			mOffset = xOffset * 100f;
			switch ((int) mOffset) {
			case 0:
				// screen at far left
				break;
			case 100:
				// screen at far right
				break;
			default:
				// screen in between somewhere
				break;
			}
			drawFrame();
		}

		@Override
		public void onTouchEvent(MotionEvent event) {
			super.onTouchEvent(event);
			switch (event.getAction()) {
			case (MotionEvent.ACTION_DOWN):
				// gradients.actionDown(event);
				break;
			case (MotionEvent.ACTION_UP):
				// rectSet.actionUp(event, plot);
				break;
			}
		}

		void drawFrame() {
			final SurfaceHolder holder = getSurfaceHolder();
			Canvas c = null;
			try {
				c = holder.lockCanvas();
				if (c != null) {
					updatePhysics();

					draw(c);
				}
			} finally {
				if (c != null)
					holder.unlockCanvasAndPost(c);
			}

			// Reschedule the next redraw
			mHandler.removeCallbacks(drawFrame);
			if (mVisible) {
				mHandler.postDelayed(drawFrame, 1000 / 25);
			}
		}

		public void draw(Canvas c) {
			background.draw(c);
			treeScene.draw(c);
			// gradients.draw(c);
			// rectSet.draw(c);
		}

		public void updatePhysics() {
			background.updatePhysics();
			// gradients.updatePhysics();
			// rectSet.updatePhysics();
		}
	}
}