package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

class TreeScene {

	float width, height;
	int centerx, centery;
	Tree tree = new Tree();

	class Tree {
		int treex, treey;
		float treeheight, mainBranchWidth;
		int branches;
		float endHeight, startHeight;
		Paint treePaint, woodPaint;

		public Tree() {
			treePaint = new Paint();
			treePaint.setColor(Color.GREEN);
			treePaint.setStyle(Style.FILL_AND_STROKE);
			treePaint.setStrokeWidth(1);

			woodPaint = new Paint();
			woodPaint.setColor(Color.DKGRAY);
			woodPaint.setStyle(Style.FILL_AND_STROKE);
			woodPaint.setStrokeWidth(1);
		}

		public void onSurfaceChanged() {
			startHeight = height * .15f;
			endHeight = height * .75f;
			mainBranchWidth = width * .25f;
			branches = (int) ((endHeight - startHeight) / 2);
		}

		public void draw(Canvas canvas) {
			for (int i = 0; i < height - endHeight; i++) {
				if (i % 2 == 0)
					canvas.drawLine(centerx - mainBranchWidth / 2, height - i,
							centerx + mainBranchWidth / 2, height - i,
							woodPaint);
			}
			float dist = 0f;
			for (float i = 0; i < tree.branches; i++) {
				switch ((int) i % 2) {
				case 0:
					dist = centerx - (centerx - i);
					// straight line to show shape of tree
					canvas.drawLine(centerx, tree.startHeight + i * 2, centerx
							+ i, tree.startHeight + i * 2, tree.treePaint);

					// far reaching branch
					canvas.drawLine(centerx, tree.startHeight + i * 2, centerx
							+ i, tree.startHeight + i * 2 - i / 3,
							tree.treePaint);

					canvas.drawLine(centerx + (dist * .5f), tree.startHeight
							+ i * 2, centerx + (dist * .5f) + i / 2,
							tree.startHeight + i * 2 - i / 3, tree.treePaint);

					// second branching section
					canvas.drawLine(centerx + (dist * .2f), tree.startHeight
							+ (i * 2), centerx + (dist * .2f) + i / 3,
							tree.startHeight + (i * 2) - (i * .5f),
							tree.treePaint);

					canvas.drawLine(centerx + (dist * .3f), tree.startHeight
							+ (i * 2), centerx + (dist * .2f) + i / 3,
							tree.startHeight + (i * 2) - (i * .5f),
							tree.treePaint);

					canvas.drawLine(centerx, tree.startHeight + (i * 2),
							centerx - (dist * .5f) + i, tree.startHeight
									+ (i * 2) + (i * .3f), tree.treePaint);

					break;
				case 1:
					dist = centerx - (centerx - i);
					// straight line to show shape of tree
					canvas.drawLine(centerx, tree.startHeight + i * 2, centerx
							- i, tree.startHeight + i * 2, tree.treePaint);

					// start of angled branches
					canvas.drawLine(centerx, tree.startHeight + i * 2, centerx
							- i, tree.startHeight + i * 2 - i / 3,
							tree.treePaint);

					canvas.drawLine(centerx - (int) (dist * .5),
							tree.startHeight + i * 2, centerx
									- (int) (dist * .5) - i / 2,
							tree.startHeight + i * 2 - i / 3, tree.treePaint);

					canvas.drawLine(centerx - (dist * .2f), tree.startHeight
							+ (i * 2), centerx - (dist * .2f) - i / 3,
							tree.startHeight + (i * 2) - (i / 2),
							tree.treePaint);

					canvas.drawLine(centerx + (dist * .3f), tree.startHeight
							+ (i * 2), centerx + (dist * .2f) + i / 3,
							tree.startHeight + (i * 2) - (i / 2),
							tree.treePaint);

					canvas.drawLine(centerx, tree.startHeight + (i * 2),
							centerx + (dist * .5f) - i, tree.startHeight
									+ (i * 2) + (i * .3f), tree.treePaint);

					break;
				}
			}
		}
	}

	public void updatePhysics() {

	}

	public void draw(Canvas canvas) {
		tree.draw(canvas);
	}

	public void onSurfaceChanged(PlotPoints plot, int w, int h) {
		width = w;
		height = h;
		centerx = w / 2;
		centery = h / 2;
		tree.onSurfaceChanged();
	}

	public void createSnow() {

	}
}