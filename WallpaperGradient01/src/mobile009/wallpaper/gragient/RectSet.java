package mobile009.wallpaper.gragient;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.MotionEvent;

public class RectSet {

	final double SIZE_RATIO = .15;
	final double SPEED_RATIO = .05;
	final int MAX = 50;
	final int[] COLORS = { Color.RED, Color.GREEN, Color.YELLOW, Color.MAGENTA };
	MyRectF[] myRectFs = new MyRectF[MAX];
	boolean[] exists = new boolean[MAX];
	int screenW, screenH, side;
	PlotPoints plot;
	int x, y;

	class MyRectF {

		RectF rect;
		Point[] rectPoints;
		Point tempPoint;
		Paint paint = new Paint();
		float currentPoint;

		public MyRectF(float left, float top, float right, float bottom) {
			rect = new RectF(left, top, right, bottom);
			paint.setColor(getColor());
			paint.setStyle(Style.FILL_AND_STROKE);
		}

		private int getColor() {
			int color = (int) (Math.random() * 100);
			return COLORS[color % COLORS.length];
		}

		public boolean updatePoint() {
			currentPoint += 5;
			if (currentPoint < rectPoints.length) {
				tempPoint = rectPoints[(int) currentPoint];
				if (tempPoint.x > 0 && tempPoint.y > 0) {
					float xdiff = tempPoint.x + (rect.right - rect.left);
					float ydiff = tempPoint.y + (rect.bottom - rect.top);
					rect.set(tempPoint.x, tempPoint.y, xdiff, ydiff);
					return true;
				}
			}
			for (int i = 0; i <= rectPoints.length / 2; i++) {
				if (rectPoints.length > 1 && i <= rectPoints.length / 2) {
					Point temp = rectPoints[i];
					rectPoints[i] = rectPoints[rectPoints.length - 1 - i];
					rectPoints[rectPoints.length - 1 - i] = temp;
				}
			}
			currentPoint = 0;
			return false;
		}
	};

	public void onSurfaceChanged(PlotPoints p, int w, int h) {
		plot = p;
		screenH = h;
		screenW = w;

		if (h > w) {
			side = (int) ((float) h * SIZE_RATIO);
		} else {
			side = (int) ((float) w * SIZE_RATIO);
		}
	}

	public void actionUp(MotionEvent event, PlotPoints plot) {
		x = (int) event.getX();
		y = (int) event.getY();
		for (int i = 0; i < exists.length; i++) {
			if (!exists[i]) {
				myRectFs[i] = new MyRectF(x - side / 2, y - side / 2, x + side
						/ 2, y + side / 2);
				exists[i] = true;
				int newx = (int) (screenW * Math.random());
				int newy = (int) (screenH * Math.random());
				myRectFs[i].rectPoints = plot.plotLine(new Point(x, y),
						new Point(newx, newy));
				return;
			}
		}
	}

	public void draw(Canvas canvas) {
		for (int i = 0; i < exists.length; i++) {
			if (exists[i])
				canvas.drawRect(myRectFs[i].rect, myRectFs[i].paint);
		}
	}

	public void updatePhysics() {
		for (int i = 0; i < exists.length; i++) {
			if (exists[i]) {
				myRectFs[i].updatePoint();
			}
		}
	}
}