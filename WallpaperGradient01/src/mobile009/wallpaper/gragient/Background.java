package mobile009.wallpaper.gragient;

import android.graphics.Canvas;

public interface Background {
	void draw(Canvas c);
	void updatePhysics();
}